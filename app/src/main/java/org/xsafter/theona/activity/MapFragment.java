package org.xsafter.theona.activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.xsafter.theona.BuildConfig;
import org.xsafter.theona.R;

public class MapFragment extends Fragment {
    MapView map;
    FusedLocationProviderClient mFusedLocationClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        map = MapView.findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        IMapController mapController = map.getController();
        mapController.setZoom(6);
        GeoPoint startPoint = new GeoPoint(59.939880,30.300718);
        mapController.setCenter(startPoint);

        return inflater.inflate(R.layout.fragment_map, container, false);
    }
}