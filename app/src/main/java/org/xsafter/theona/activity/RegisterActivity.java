package org.xsafter.theona.activity;

import androidx.appcompat.app.AppCompatActivity;
import java.util.Objects;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Button;
import android.widget.EditText;

import org.xsafter.theona.R;
import org.xsafter.theona.UtilApplication;
import org.xsafter.theona.helpers.DatabaseHelper;
import org.xsafter.theona.model.User;
import org.xsafter.theona.service.network.NetworkService;

public class RegisterActivity extends AppCompatActivity {
    EditText usernameEditText;
    EditText passwordEditText;
    EditText repeatPasswordEditText;
    Button signupButton;
    Button signinButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        linkView();

        if (DatabaseHelper.getUserSelf() == null){
            signupButton.setOnClickListener(view -> RegisterActivity.this.startActivity(new Intent(this, LoginActivity.class)));
            signinButton.setOnClickListener(view -> {
                if (checkFields()) {
                    User user = DatabaseHelper.updateSelfNickname(usernameEditText.getText().toString());

                    Intent membersIntent = new Intent(NetworkService.NETWORK_BROADCAST_KEY);
                    membersIntent.putExtra(NetworkService.BROADCAST_MESSAGE_TYPE_KEY, NetworkService.BROADCAST_ACTION.REFRESH_MEMBERS_KEY);
                    LocalBroadcastManager.getInstance(UtilApplication.getInstance()).sendBroadcast(membersIntent);
                    Intent chatIntent = new Intent(NetworkService.NETWORK_BROADCAST_KEY);
                    chatIntent.putExtra(NetworkService.BROADCAST_MESSAGE_TYPE_KEY, NetworkService.BROADCAST_ACTION.REFRESH_CHAT_KEY);
                    LocalBroadcastManager.getInstance(UtilApplication.getInstance()).sendBroadcast(chatIntent);

                    Intent serviceIntent = new Intent(UtilApplication.getInstance(), NetworkService.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(NetworkService.MINIFIED_OBJECT_SERIALIZABLE_EXTRA_KEY, user.prepare());
                    serviceIntent.putExtras(bundle);
                    serviceIntent.setAction(NetworkService.ACTION.SEND_MULTICAST_ACTION);
                    UtilApplication.getInstance().startService(serviceIntent);
                }
            });
        } else {
            RegisterActivity.this.startActivity(new Intent(this, LoginActivity.class));
        }
    }

    private void linkView(){
        usernameEditText = findViewById(R.id.usernameEditText);
        signupButton = findViewById(R.id.signupButton);
    }

    public boolean checkFields(){
        if (usernameEditText == null || usernameEditText.getText().toString().isEmpty()) {
            usernameEditText.setError("username is empty!");
            return false;
        }
        if (DatabaseHelper.getUserByNickname(usernameEditText.getText().toString()) != null) {
            usernameEditText.setError("username is already taken!");
            return false;
        }
        return true;
    }
}