package org.xsafter.theona.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DrawableRound extends Drawable {
    private final Bitmap bitmap;
    private final Paint paint;
    private final RectF rectangle;
    private final int width;
    private final int height;

    public DrawableRound(Bitmap bitmap) {
        this.bitmap = bitmap;
        this.paint = new Paint();
        this.width = bitmap.getWidth();
        this.height = bitmap.getHeight();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

        this.rectangle = new RectF();
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawOval(rectangle, paint);
    }

    @Override
    public void setAlpha(int i) {
        if (paint.getAlpha() != i) {
            paint.setAlpha(i);
            invalidateSelf();
        }
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        paint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    @Override
    public void setFilterBitmap(boolean filter) {
        super.setFilterBitmap(filter);
        invalidateSelf();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public int getIntrinsicWidth() {
        return width;
    }

    @Override
    public int getIntrinsicHeight() {
        return height;
    }
}
