package org.xsafter.theona.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.xsafter.theona.R;
import org.xsafter.theona.model.User;

import java.util.List;

public class ContactsRecyclerAdapter extends RecyclerView.Adapter<ContactsRecyclerAdapter.ContactItemViewHolder> {
    protected final List<User> contacts;

    public ContactsRecyclerAdapter(List<User> contacts) {
        this.contacts = contacts;
    }

    @NonNull
    @Override
    public ContactItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new ContactItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactItemViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class ContactItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        final ImageView avatarImageView;
        final TextView title;
        final TextView subTitle;

        public ContactItemViewHolder(@NonNull View itemView) {
            super(itemView);

            avatarImageView = (ImageView) itemView.findViewById(R.id.avatarImageView);
            title = (TextView) itemView.findViewById(R.id.listitem_text);
            subTitle = (TextView) itemView.findViewById(R.id.listitem_nickname);

            itemView.setOnClickListener(this);
            subTitle.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
