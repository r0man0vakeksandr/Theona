package org.xsafter.theona;

import android.app.Application;
import android.content.Intent;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.BuildConfig;

import org.xsafter.theona.helpers.SharedPreferencesHelper;
import org.xsafter.theona.service.network.NetworkService;

/**
 * The main reason why class extends {@link Application} is init of {@link ActiveAndroid} DB*/
public class UtilApplication extends Application {
    private static SharedPreferencesHelper sharedPreferencesHelper;
    private static UtilApplication instance;

    public UtilApplication() {super();}

    public static UtilApplication getInstance() {return instance;}

    public static SharedPreferencesHelper getSharedPreferencesHelper() {
        return sharedPreferencesHelper;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        ActiveAndroid.setLoggingEnabled(BuildConfig.DEBUG);
        sharedPreferencesHelper = new SharedPreferencesHelper(this);
        instance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Intent serviceIntent = new Intent(getInstance(), NetworkService.class);
        serviceIntent.setAction(NetworkService.ACTION.STOP_FOREGROUND_ACTION);
        getInstance().startService(serviceIntent);
    }
}
