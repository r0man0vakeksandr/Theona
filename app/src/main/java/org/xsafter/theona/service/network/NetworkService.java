package org.xsafter.theona.service.network;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;

import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.activeandroid.query.Select;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AppIdentifier;
import com.google.android.gms.nearby.connection.AppMetadata;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.Connections;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;

import org.xsafter.theona.R;
import org.xsafter.theona.UtilApplication;
import org.xsafter.theona.helpers.NetworkHelper;
import org.xsafter.theona.model.Message;
import org.xsafter.theona.model.MessageReady;
import org.xsafter.theona.model.MessageWrapped;
import org.xsafter.theona.model.User;
import org.xsafter.theona.model.UserReady;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * <pre>class NetworkService - Service for connecting devices via <b>gms.nearby</b>.
 * Implements GoogleApi, Nearby.Connection
 * Receives & sends Payloads:
 *   >Message
 *   >User
 *   >TechnicalMessage
 * Implements Discovery and Advertise model.
 *
 * Class implements ConnectionLifecycleCallback, PayloadCallback, EndpointDiscoveryCallback<pre>
 */

public class NetworkService extends Service {

    /** Static vars for config*/
    public static final String BROADCAST_MESSAGE_TYPE_KEY = "BROADCAST_MESSAGE_TYPE";
    public static final String ACTIVITY_FOREGROUND_KEY = "ACTIVITY_FOREGROUND";
    public static final String MINIFIED_OBJECT_SERIALIZABLE_EXTRA_KEY = "MINIFIED_OBJECT_SERIALIZABLE_EXTRA";
    public static final String NETWORK_BROADCAST_KEY = "NETWORK_BROADCAST";
    // Set an appropriate timeout length in milliseconds
    private static final long DISCOVER_TIMEOUT = 10000L;
    private static final long CONNECT_TIMEOUT = 10000L;
    private boolean isConnected = true;
    private boolean isStarted = false;
    private boolean isInitializing = false;
    private boolean isActivity = false;
    private int initialSyncDataCount = 0;
    private int initialSyncTotalDataCount = -1;

    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> discoveringScheduledFuture;
    private ScheduledFuture<?> connectingScheduledFuture;
    private GoogleApiClient mGoogleApiClient;
    private NotificationCompat.Builder mNotificationBuilder;
    private PowerManager.WakeLock mWakeLock;
    private String localDeviceId;
    private String localEndpointId;
    private int outgoingTotalDataCount;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() { //FIXME
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Nearby.CONNECTIONS_API)
                .build();
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, UtilApplication.getInstance().getString(R.string.app_name));
        if (null != mWakeLock && !mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
    }



    public void onConnectionInitiated(@NonNull String s, @NonNull ConnectionInfo connectionInfo) {

    }

    public void onConnectionResult(@NonNull String s, @NonNull ConnectionResolution connectionResolution) {

    }

    public void onDisconnected(@NonNull String s) {
        mGoogleApiClient.reconnect();
    }

    public void onPayloadReceived(@NonNull String s, @NonNull Payload payload) {

    }

    public void onPayloadTransferUpdate(@NonNull String s, @NonNull PayloadTransferUpdate payloadTransferUpdate) {

    }

    public void onEndpointFound(@NonNull String s, @NonNull DiscoveredEndpointInfo discoveredEndpointInfo) {
        isConnected = true; //flag to know if mesh endpoint found
        String endpointInfo = Arrays.toString(discoveredEndpointInfo.getEndpointInfo());
        final String endpointName = discoveredEndpointInfo.getEndpointName();
        connectTo(endpointInfo, endpointName);

        if (discoveringScheduledFuture != null) {
            discoveringScheduledFuture.cancel(true); //Signal that task is no longer needed
            discoveringScheduledFuture = null;
        }
    }

    private PayloadCallback payloadCallback = new PayloadCallback() {
        @Override
        public void onPayloadReceived(@NonNull String s, @NonNull Payload payload) {
            Map<Byte, Object> parsedPayload = new HashMap<>();
            payload.getId();

            try {
                parsedPayload = NetworkHelper.toMap(payload.asBytes());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            Map.Entry<Byte, Object> entry = parsedPayload.entrySet().iterator().next();

            switch (entry.getKey()) {
                case NETWORK_MESSAGE_TYPE.TECHNICAL_MESSAGE:
                    MessageWrapped messageWrapped = new MessageWrapped(entry.getValue());
                    switch (messageWrapped.getMessageRaw()) {
                        case NETWORK_ACTION.GET_ALL_DATA:
                            outgoingTotalDataCount = 0;
                            List<User> users = new Select().from(User.class).execute();
                            for (User user : users) {
                                byte[] myPayload = new byte[0];
                                try {
                                    UserReady minifiedUser = user.prepare();
                                    myPayload = NetworkHelper.toBytes(minifiedUser);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                Nearby.Connections.sendReliableMessage(mGoogleApiClient,
                                        endpointId,
                                        myPayload);
                                outgoingTotalDataCount++;
                            }

                            List<Message> messages = new Select().from(Message.class).execute();
                            for (Message message : messages) {
                                sendMessage(message, endpointId);
                                outgoingTotalDataCount++;
                            }
                            MessageWrapped totalDataCountMessage = new MessageWrapped(NETWORK_ACTION.POST_TOTAL_DATA_COUNT);
                            totalDataCountMessage.setMetadata(String.valueOf(outgoingTotalDataCount));
                            byte[] myPayload = null;
                            try {
                                myPayload = NetworkHelper.toBytes(totalDataCountMessage);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Nearby.Connections.sendReliableMessage(mGoogleApiClient,
                                    endpointId,
                                    myPayload);
                            break;
                        case NETWORK_ACTION.GET_FRESH_DATA:
                            break;
                        case NETWORK_ACTION.POST_TOTAL_DATA_COUNT:
                            initialSyncTotalDataCount = Integer.parseInt(messageWrapped.getMessageRaw());
                            break;
                    }
                    break;
                case NETWORK_MESSAGE_TYPE.MESSAGE:
                    MessageReady minifiedMessage = (MessageReady) entry.getValue();
                    DBHelper.addMessage(minifiedMessage);
                    if (!isInitializing) {
                        updateNotification(minifiedMessage);
                    }
                    Intent chatIntent = new Intent(NETWORK_BROADCAST_KEY);
                    chatIntent.putExtra(NetworkService.BROADCAST_MESSAGE_TYPE_KEY, BROADCAST_ACTION.REFRESH_CHAT_KEY);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(chatIntent);
                    if (isInitializing) initialSyncDataCount++;
                    break;
                case NETWORK_MESSAGE_TYPE.USER_UPDATE:
                    UserReady minifiedUser = (UserReady) entry.getValue();
                    DBHelper.addUser(minifiedUser);
                    Intent membersIntent = new Intent(NETWORK_BROADCAST_KEY);
                    membersIntent.putExtra(NetworkService.BROADCAST_MESSAGE_TYPE_KEY, BROADCAST_ACTION.REFRESH_MEMBERS_KEY);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(membersIntent);
                    if (isInitializing) initialSyncDataCount++;
                    break;
            }
            if (isInitializing) {
                if (initialSyncTotalDataCount == initialSyncDataCount) {
                    DBHelper.createSelfUser(localDeviceId, localEndpointId);

                    Intent serviceIntent = new Intent(NetworkService.this, NetworkService.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(NetworkService.MINIFIED_OBJECT_SERIALIZABLE_EXTRA_KEY, DBHelper.getSelfUser().minified());
                    serviceIntent.putExtras(bundle);
                    serviceIntent.setAction(NetworkService.ACTION.SEND_MULTICAST_ACTION);
                    startService(serviceIntent);
                    unblockUI();
                    isInitializing = false;
                }
            }
        }

        @Override
        public void onPayloadTransferUpdate(@NonNull String s, @NonNull PayloadTransferUpdate payloadTransferUpdate) {

        }
    };



    public interface ACTION {
        String MAIN_ACTION = "org.xsafter.theona.action.main";
        String STOP_FOREGROUND_ACTION = "org.xsafter.theona.action.stop_foreground";
        String UPDATE_ACTION = "org.xsafter.theona.action.update";
        String SEND_MULTICAST_ACTION = "org.xsafter.theona.action.send_multicast";
        String START_FOREGROUND_ACTION = "org.xsafter.theona.action.start_foreground";
    }

    public interface NETWORK_ACTION {
        String GET_FRESH_DATA = "org.xsafter.theona.network_action.get_fresh_data";
        String GET_ALL_DATA = "org.xsafter.theona.network_action.get_all_data";
        String POST_TOTAL_DATA_COUNT = "org.xsafter.theona.network_action.post_total_data_count";
    }

    public interface BROADCAST_ACTION {
        String REFRESH_CHAT_KEY = "REFRESH_CHAT";
        String REFRESH_MEMBERS_KEY = "REFRESH_MEMBERS";
        String REFRESH_USER_PROFILE_KEY = "REFRESH_USER_PROFILE";
        String NETWORK_INITIALISATION_COMPLETED_KEY = "NETWORK_INITIALISATION_COMPLETED";
    }

    public interface NETWORK_MESSAGE_TYPE {
        byte TECHNICAL_MESSAGE = 10;
        byte MESSAGE = 20;
        byte USER_UPDATE = 30;
    }

    public interface NOTIFICATION_ID {
        int FOREGROUND_SERVICE = 101;
    }

}