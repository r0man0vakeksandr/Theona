package org.xsafter.theona.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesHelper {
    private final SharedPreferences preferences;

    public SharedPreferencesHelper(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void clearAll() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();
    }

    public SharedPreferences getDefaultSharedPreferences() {
        return preferences;
    }

    public long getLastUpdateTime() {
        return preferences.getLong("LAST_UPDATE_TIME", -1);
    }

    public void setLastUpdateTime(long time) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("LAST_UPDATE_TIME", time);
        editor.apply();
    }

    public String getActiveRemoteEndpointId() {
        return preferences.getString("ACTIVE_REMOTE_ENDPOINT_ID_KEY", "");
    }


    public void setActiveRemoteEndpointId(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ACTIVE_REMOTE_ENDPOINT_ID_KEY", value);
        editor.apply();
    }
}
