package org.xsafter.theona.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import org.xsafter.theona.model.User;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.UUID;

public class DatabaseHelper {

    public static User getUserSelf() {
        return new Select().from(User.class).where(User.COLUMN_IS_SELF + " = ?", true).executeSingle();
    }

    public static void createUserSelf(String endpointId, String deviceId) {
        User userSelf = getUserSelf();
        if (userSelf == null) {
            ActiveAndroid.beginTransaction();
            User user = new User();
            user.setModified(System.currentTimeMillis()); user.setSelf(true); user.setUuid(UUID.randomUUID().toString()); user.setRemoteEndpointId(endpointId); user.setNickname((deviceId + endpointId).substring(0, 20));
            user.save();
            ActiveAndroid.endTransaction();
        }
    }

    public static User getUserByUUID(String uuid) {
        return new Select().from(User.class).where(User.COLUMN_UUID + " = ?", uuid).executeSingle();
    }

    public static User getUserByNickname(String nickname) {
        return new Select().from(User.class).where(User.COLUMN_NICKNAME + " = ?", nickname).executeSingle();
    }

    public static List<User> getAvailableUsers() {
        return new Select().from(User.class).execute();
    }

    public static User updataSelfAvatar(Bitmap bitmapAvatar) {
        User userSelf = getUserSelf();
        ActiveAndroid.beginTransaction();
        userSelf.setAvatar(bitMapToBase64String(bitmapAvatar));
        userSelf.save();
        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
        return userSelf;
    }

    public static User updateSelfNickname(String nickname) {
        User selfUser = getUserSelf();
        ActiveAndroid.beginTransaction();
        selfUser.setNickname(nickname);
        selfUser.save();
        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
        return selfUser;
    }

    private static String bitMapToBase64String(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        byte[] b = outputStream.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static Bitmap base64StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }
}
