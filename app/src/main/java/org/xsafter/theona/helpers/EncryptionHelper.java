package org.xsafter.theona.helpers;

import android.os.Build;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import org.jetbrains.annotations.TestOnly;

import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
/**
 * AES Encoding with Cipher Block Chaining and Password-based Encryption Standard
 *
 * */
public class EncryptionHelper {
    private static final String ALGORITHM = "AES";
    private static final String PASSWORD = "LAt6yJA5i15H1E6g";
    private static final String SALT = "_salt*";

    private static final String KEYGEN_SPEC = "PBKDF2WithHmacSHA1";
    private static final String CIPHER_SPEC = "AES/CBC/PKCS5Padding";

    public static String encrypt(String clearText) {
        Key key;
        Cipher cipher;
        try {
            key = getKey();
            cipher = Cipher.getInstance(CIPHER_SPEC);
            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(new byte[16]));
            byte[] encVal = cipher.doFinal(clearText.getBytes());
            return Base64.encodeToString(encVal, Base64.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String decrypt(String encryptedText) {
        Key key;
        try {
            key = getKey();
            Cipher cipher = Cipher.getInstance(CIPHER_SPEC);
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
            byte[] decordedValue = Base64.decode(encryptedText, Base64.DEFAULT);
            byte[] decValue = cipher.doFinal(decordedValue);
            String decryptedValue = new String(decValue);
            return decryptedValue;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Key getKey() throws Exception {
        SecretKeyFactory factory = SecretKeyFactory.getInstance(KEYGEN_SPEC);
        KeySpec spec = new PBEKeySpec(PASSWORD.toCharArray(), SALT.getBytes(), 65536, 128);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);
        return secret;
    }
}
