package org.xsafter.theona.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.xsafter.theona.model.MessageReady;
import org.xsafter.theona.model.MessageWrapped;
import org.xsafter.theona.model.UserReady;
import org.xsafter.theona.service.network.NetworkService;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;

public class NetworkHelper {

    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null &&
                manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting())
                || (manager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET) != null &&
                manager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET).isConnectedOrConnecting())){
            return true;
        }
        return false;
    }

    public static byte[] toBytes(Object object) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos);
        out.writeObject(object);
        byte[] byteArrayObject = bos.toByteArray();

        byte a = 0;
        if (object instanceof MessageWrapped)
            a = NetworkService.NETWORK_MESSAGE_TYPE.TECHNICAL_MESSAGE;
        else if (object instanceof MessageReady)
            a = NetworkService.NETWORK_MESSAGE_TYPE.MESSAGE;
        else if (object instanceof UserReady)
            a = NetworkService.NETWORK_MESSAGE_TYPE.USER_UPDATE;

        if (0 != a) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(a);
            outputStream.write(byteArrayObject);
            return outputStream.toByteArray();
        } else {
            return null;
        }
    }

    public static HashMap<Byte, Object> toMap(byte[] bytes) throws IOException, ClassNotFoundException {

        byte technicalByte = bytes[0];
        byte[] filteredByteArray = Arrays.copyOfRange(bytes, 1, bytes.length);

        ByteArrayInputStream bis = new ByteArrayInputStream(filteredByteArray);
        ObjectInput in = new ObjectInputStream(bis);
        HashMap<Byte, Object> payload = new HashMap<>();
        Object object = in.readObject();
        in.close();
        payload.put(technicalByte, object);
        return payload;
    }
}
