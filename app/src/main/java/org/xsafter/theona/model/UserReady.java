package org.xsafter.theona.model;

import java.io.Serializable;

public class UserReady implements Serializable {
    public final String nickname;
    public final long modified;
    public final String avatar;
    public final String uuid;
    public final String remoteEndpointId;

    public UserReady(User user) {
        this.nickname = user.getNickname();
        this.modified = user.getModified();
        this.avatar = user.getAvatar();
        this.uuid = user.getUuid();
        this.remoteEndpointId = user.getRemoteEndpointId();
    }

    public UserReady(Object object) {
        UserReady incomingUser = (UserReady) object;
        this.nickname = incomingUser.nickname;
        this.modified = incomingUser.modified;
        this.avatar = incomingUser.avatar;
        this.uuid = incomingUser.uuid;
        this.remoteEndpointId = incomingUser.remoteEndpointId;
    }
}
