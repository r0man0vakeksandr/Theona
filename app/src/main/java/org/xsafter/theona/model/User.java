package org.xsafter.theona.model;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "User", id = BaseColumns._ID)
public class User extends Model {
    public static final String COLUMN_IS_SELF = "IsSelf";
    public static final String COLUMN_UUID = "UUID";
    private static final String COLUMN_REMOTE_ENDPOINT_ID = "RemoteEndpointId";
    private static final String COLUMN_MODIFIED = "Modified";

    public static final String COLUMN_NICKNAME = "Name";
    private static final String COLUMN_STATUS = "Status";
    private static final String COLUMN_AVATAR = "Avatar";
    @Column(name = COLUMN_MODIFIED)
    private long modified;
    @Column(name = COLUMN_STATUS)
    private String status;
    @Column(name = COLUMN_NICKNAME, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String nickname;
    @Column(name = COLUMN_AVATAR)
    private String avatar;
    @Column(name = COLUMN_IS_SELF)
    private boolean isSelf = false;
    @Column(name = COLUMN_UUID)
    private String uuid;
    @Column(name = COLUMN_REMOTE_ENDPOINT_ID)
    private String remoteEndpointId;

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean self) {
        this.isSelf = self;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getRemoteEndpointId() {
        return remoteEndpointId;
    }

    public void setRemoteEndpointId(String remoteEndpointId) {
        this.remoteEndpointId = remoteEndpointId;
    }

    public UserReady prepare(){
        return new UserReady(this);
    }
}
