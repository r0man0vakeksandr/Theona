package org.xsafter.theona.model;

import java.io.Serializable;

/**
 * Prepared Message ready to be sent by Nearby using Serializable
 * */
public class MessageReady implements Serializable {

    public final String uuid;
    public final long timeCreated;
    public final String text;

    protected MessageReady(String uuid, long timeCreated, String text) {
        this.uuid = uuid;
        this.timeCreated = timeCreated;
        this.text = text;
    }

    public MessageReady(Message message) {
        this.uuid = message.getUuid();
        this.timeCreated = message.getCreated();
        this.text = message.getMessageText();
    }

}
