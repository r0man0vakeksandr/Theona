package org.xsafter.theona.model;

import java.io.Serializable;

public class MessageWrapped  implements Serializable {
    private String metadata;
    private long time;
    private final String messageRaw;

    public MessageWrapped(String messageRaw) {
        this.messageRaw = messageRaw;
    }

    public MessageWrapped(Object object) {
        MessageWrapped incomingMessage = (MessageWrapped) object;
        this.messageRaw = incomingMessage.getMessageRaw();
        this.metadata = incomingMessage.getMetadata();
    }

    public String getMetadata() {
        return metadata;
    }

    public long getTime() {
        return time;
    }

    public String getMessageRaw() {
        return messageRaw;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
}
