package org.xsafter.theona.model;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

/**
 * DB Model of Custom Message via activeandroid
 * */
@Table(name = "message", id = BaseColumns._ID)
public class Message extends Model {
    private static final String COLUMN_UUID = "UUID";
    private static final String COLUMN_CREATED = "Created";
    private static final String COLUMN_TEXT = "Text";

    @Column(name = COLUMN_UUID, uniqueGroups = {"single-user-message"}, onUniqueConflicts = {Column.ConflictAction.IGNORE})
    private String uuid;
    @Column(name = COLUMN_CREATED, uniqueGroups = {"single-user-message"}, onUniqueConflicts = {Column.ConflictAction.IGNORE})
    private long created;
    @Column(name = COLUMN_TEXT)
    private String messageText;

    public Message() {
        super();
    }

    protected Message(String uuid, long created, String messageText) {
        this.uuid = uuid;
        this.created = created;
        this.messageText = messageText;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public MessageWrapped minifiy() {
        return new MessageWrapped(this);
    }
}
