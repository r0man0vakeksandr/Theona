package org.xsafter.theona.model;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

@Table(name = "geopoint", id = BaseColumns._ID)
public class GeoMessage extends Model implements Serializable {
    private final String COLUMN_UUID = "UUID";
    private final String COLUMN_CREATED = "Created";
    private final String COLUMN_LATITUDE = "Latitude";
    private final String COLUMN_LONGITUDE = "Longitude";

    @Column(name = COLUMN_UUID, uniqueGroups = {"single-user-message"}, onUniqueConflicts = {Column.ConflictAction.IGNORE})
    public String uuid;
    @Column(name = COLUMN_CREATED, uniqueGroups = {"single-user-message"}, onUniqueConflicts = {Column.ConflictAction.IGNORE})
    public long created;
    @Column(name = COLUMN_LONGITUDE, uniqueGroups = {"single-user-message"}, onUniqueConflicts = {Column.ConflictAction.IGNORE})
    public long longitude;
    @Column(name = COLUMN_LATITUDE, uniqueGroups = {"single-user-message"}, onUniqueConflicts = {Column.ConflictAction.IGNORE})
    public long latitude;

    public GeoMessage(){
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getLongitude() {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }

    public long getLatitude() {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }
}
